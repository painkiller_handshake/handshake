--NCRHACKCUSTRETRIG
--NCR ERP Customer Re-trigger Program

CREATE OR REPLACE PROCEDURE ncr_erp_hack_retrigger_proc 
(
errbuf OUT VARCHAR2
,retcode OUT NUMBER
)
IS

CURSOR c1 IS
select rowid,a.* from ncr_soupuser.ncr_erp_hack_retrigger_tbl a
WHERE Processing_status ='R';

lv_already NUMBER;

BEGIN

FOR c1_rec IN c1 LOOP

IF c1_rec.erp_site_number IS NOT NULL THEN

SELECT count(1) INTO lv_already
FROM ncr_soupuser.ncr_erp_hack_retrigger_tbl
WHERE erp_customer_number = c1_rec.erp_customer_number AND erp_site_number = c1_rec.erp_site_number
AND Processing_status='P' AND to_date(creation_date,'DD-MON-YY HH24:MI:SS')>(sysdate-1);

IF lv_already = 0 THEN  ---1
UPDATE hz_parties 
SET last_update_date = last_update_date
WHERE party_number = c1_rec.erp_customer_number;

UPDATE hz_cust_accounts 
SET last_update_date = last_update_date
WHERE account_number = c1_rec.erp_customer_number;

UPDATE hz_party_sites
SET last_update_date = last_update_date
WHERE party_site_number =  c1_rec.erp_site_number;

UPDATE hz_cust_acct_sites_all
SET last_update_date = last_update_date
WHERE party_site_id IN (select party_site_id from hz_party_sites 
						where party_site_number= c1_rec.erp_site_number);

UPDATE hz_cust_site_uses_all
SET last_update_date = last_update_date
WHERE cust_acct_site_id IN ( select cust_acct_site_id from hz_cust_acct_sites_all WHERE party_site_id IN
						 (select party_site_id from hz_party_sites 
						where party_site_number= c1_rec.erp_site_number)
						);

UPDATE ncr_soupuser.ncr_erp_hack_retrigger_tbl
SET Processing_status = 'D'
WHERE rowid= c1_rec.rowid;

END IF; --1

ELSE

SELECT count(1) INTO lv_already
FROM ncr_soupuser.ncr_erp_hack_retrigger_tbl
WHERE erp_customer_number = c1_rec.erp_customer_number AND erp_site_number IS NULL
AND Processing_status='P' AND to_date(creation_date,'DD-MON-YY HH24:MI:SS')>(sysdate-1);

IF lv_already = 0 THEN  ---1

UPDATE hz_parties 
SET last_update_date = last_update_date
WHERE party_number = c1_rec.erp_customer_number;

UPDATE hz_cust_accounts 
SET last_update_date = last_update_date
WHERE account_number = c1_rec.erp_customer_number;

UPDATE hz_party_sites
SET last_update_date = last_update_date
WHERE 
status='A'
AND party_id IN (
SELECT party_id FROM hz_parties WHERE party_number = c1_rec.erp_customer_number
				   )
and rownum<5000;

UPDATE hz_cust_acct_sites_all
SET last_update_date = last_update_date
WHERE 
status='A'
AND party_site_id IN (select party_site_id from hz_party_sites 
						WHERE party_id IN (
						SELECT party_id FROM hz_parties 
						WHERE party_number = c1_rec.erp_customer_number
				        ))and rownum<5000;

UPDATE hz_cust_site_uses_all
SET last_update_date = last_update_date
WHERE 
status='A'
AND cust_acct_site_id IN ( select cust_acct_site_id from hz_cust_acct_sites_all WHERE party_site_id IN
						 (select party_site_id from hz_party_sites 
						WHERE party_id IN (
						SELECT party_id FROM hz_parties 
						WHERE party_number = c1_rec.erp_customer_number
				        ))
						)and rownum<5000;


UPDATE ncr_soupuser.ncr_erp_hack_retrigger_tbl
SET Processing_status = 'D'
WHERE rowid= c1_rec.rowid;

END IF; ---01

END IF;

DECLARE
v_email_id varchar2(200);
v_email_ret varchar2(200);
v_err_msg varchar2(200);
v_attachment_list NCR_SEND_ATTACH_PKG.ATTACHMENTS_LIST;
BEGIN

IF lv_already = 0 THEN
 v_email_id    := NVL(c1_rec.quicklook_id,'vs250016@ncr.com');
        v_email_ret := NCR_SEND_ATTACH_PKG.SendMail ( 
		SMTPServerName => SYS_CONTEXT ('USERENV', 'SERVER_HOST')|| '.daytonoh.ncr.com', 
		Sender => 'WE230003@ncr.com', 
		Recipient => v_email_id, 
		CcRecipient => 'vs250016@ncr.com', 
        BccRecipient => '', 
		Subject => 'Customer Sites Information Re-triggered from ERP', 
		Body => 'The customer '||c1_rec.erp_customer_number||' and its Site has been re-triggered from ERP', 
		ErrorMessage => v_err_msg,
		Attachments => v_attachment_list);
ELSE
 v_email_id    := NVL(c1_rec.quicklook_id,'vs250016@ncr.com');
        v_email_ret := NCR_SEND_ATTACH_PKG.SendMail ( 
		SMTPServerName => SYS_CONTEXT ('USERENV', 'SERVER_HOST')|| '.daytonoh.ncr.com', 
		Sender => 'WE230003@ncr.com', 
		Recipient => v_email_id, 
		CcRecipient => 'vs250016@ncr.com', 
        BccRecipient => '', 
		Subject => 'Customer Sites Information Re-triggered from ERP Status', 
		Body => 'The customer '||c1_rec.erp_customer_number||' and its Site has already been re-triggered from ERP within 24 Hours. Your Request is Cancelled', 
		ErrorMessage => v_err_msg,
		Attachments => v_attachment_list);
END IF;
EXCEPTION
WHEN OTHERS THEN
dbms_output.put_line('Error '||sqlerrm);
END;

UPDATE ncr_soupuser.ncr_erp_hack_retrigger_tbl
SET Processing_status = 'P'
WHERE rowid= c1_rec.rowid;


END LOOP;
EXCEPTION
WHEN OTHERS THEN
fnd_file.put_line(fnd_file.LOG, ' Error in Main PROCEDURE');
END ncr_erp_hack_retrigger_proc;

/

