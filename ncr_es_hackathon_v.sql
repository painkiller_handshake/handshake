create or replace view ncr_es_hackathon_v (
es_customer_name,
es_customer_number,
es_site_number,
es_customer_status,
es_site_status
) AS
(
select 
hp.party_name ,
hca.account_number,
hps.party_site_number,
hca.status,
hzcas.status
from
hz_parties hp ,hz_cust_accounts hca,hz_party_sites hps,hz_cust_acct_sites_all hzcas
WHERE
hp.party_id=hps.party_id
and hp.party_id=hca.party_id
and hps.party_site_id=hzcas.party_site_id
and hca.cust_account_id=hzcas.cust_account_id
);

--- <soap:address location="http://153.58.57.37:8888/Application11-Get_es_Customer_Data-context-root/esCustomerWebServiceSoapHttpPort"/>
     