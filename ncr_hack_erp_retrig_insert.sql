create or replace procedure ncr_hack_erp_retrig_insert
(
p_erp_customer_number VARCHAR2,
p_erp_site_number VARCHAR2,
p_processing_status VARCHAR2,
p_quicklook_id VARCHAR2,
p_creation_date VARCHAR2 DEFAULT null
)
IS
BEGIN
insert into ncr_soupuser.ncr_erp_hack_retrigger_tbl values
(
p_erp_customer_number,
p_erp_site_number,
p_processing_status,
p_quicklook_id,
to_char(sysdate,'DD-MON-YY HH24:MI:SS')
);

END ncr_hack_erp_retrig_insert;
/