/* ******************************************************************************************* */
/* ********************************** Customer Details *****************************************/
/* ******************************************************************************************* */
select	    c.customer_name,  c.customer_nbr,  Null as SITE_NUMBER,  decode(rtrim(c.status_code),
        'A', 'Active',
        'I',
        'Inactive') as CUSTOMER_STATUS,
        count(distinct 
		case 
			when rtrim(s.site_status) = 'I' then s.customer_site_nbr 
		end) site_status,
		
        count(distinct 
		case 
			when rtrim(s.site_status) = 'A' then s.customer_site_nbr 
		end) site_nbr_count,
                count(ce.item_instance_id) equipment_count  
from	      tedw.dim_customer_mdm c  
inner join tedw.dim_cust_site_mdm s      
    on c.customer_nbr = s.customer_nbr 
    and c.data_source_code = 'ERP' 
left join tedw.es_sdi_cust_site_asset_dn ce      
    on c.customer_nbr = ce.contract_customer_nbr      
    and ce.erp_cust_site_nbr = s.customer_site_nbr  
where	    c.customer_nbr = #customer_nbr  
group by 1,2,3,4;


/* ******************************************************************************************* */
/* ********************************** Site Details ******************************************* */
/* ******************************************************************************************* */
select	  c.customer_name,  c.customer_nbr,  s.customer_site_nbr as SITE_NUMBER,
        decode(rtrim(c.status_code),
        'A', 'Active',
        'I',
        'Inactive') as CUSTOMER_STATUS,
        count(
		case 
			when s.site_status = 'I' then s.customer_site_nbr 
		end) SITE_STATUS,
		
        count(
		case 
			when s.site_status = 'A' then s.customer_site_nbr 
		end) site_nbr_count,
		     
                count(ce.item_instance_id) equipment_count  
from	      tedw.dim_customer_mdm c  
left join (
	    select	 customer_nbr, data_source_code, customer_site_nbr, site_status
	    from	   tedw.dim_cust_site_mdm 
	    where	  customer_nbr = #customer_nbr
	        and customer_site_nbr = #SITE_NUMBER
	        and data_source_code = 'ERP' 
	        ) s     
    on c.customer_nbr = s.customer_nbr 
    and c.data_source_code = 'ERP' 
    
left join tedw.es_sdi_cust_site_asset_dn ce      
    on c.customer_nbr = ce.contract_customer_nbr      
    and ce.erp_cust_site_nbr = s.customer_site_nbr  
where	c.data_source_code = 'ERP' 
	and   c.customer_nbr = #customer_nbr
group by 1,2,3,4;
