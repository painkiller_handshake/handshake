CREATE OR REPLACE PACKAGE ncr_es_customer_data
as
PROCEDURE ncr_es_fetch_customer
(
p_es_customer_name   OUT VARCHAR2,
p_es_customer_number IN  OUT VARCHAR2,
p_es_site_number  IN OUT VARCHAR2,
p_es_customer_status  OUT VARCHAR2,
p_es_site_status OUT VARCHAR2,
p_site_count OUT NUMBER,
p_ib_count OUT NUMBER
);

END ncr_es_customer_data;
/ 

CREATE OR replace PACKAGE BODY apps.ncr_es_customer_data AS 

PROCEDURE ncr_es_fetch_customer
(
p_es_customer_name   OUT VARCHAR2,
p_es_customer_number IN  OUT VARCHAR2,
p_es_site_number  IN OUT VARCHAR2,
p_es_customer_status  OUT VARCHAR2,
p_es_site_status OUT VARCHAR2,
p_site_count OUT NUMBER,
p_ib_count OUT NUMBER
)
IS
BEGIN

BEGIN
IF  p_es_customer_number IS NOT NULL AND p_es_site_number IS NULL THEN

SELECT 
es_customer_name,
es_customer_number,
null,
decode(es_customer_status,'A','Active','I','Inactive',es_customer_status)
--,null
INTO  
p_es_customer_name,
p_es_customer_number,
p_es_site_number,
p_es_customer_status
FROM ncr_es_hackathon_v  
WHERE 
es_customer_number = p_es_customer_number 
AND rownum=1;


SELECT count(es_site_number)
INTO p_site_count
FROM ncr_es_hackathon_v WHERE es_customer_number = p_es_customer_number and es_site_status = 'A';

SELECT count(es_site_number)
INTO p_es_site_status
FROM ncr_es_hackathon_v WHERE es_customer_number = p_es_customer_number and es_site_status = 'I';


SELECT count(1) INTO p_ib_count
FROM csi_item_instances WHERE instance_status_id= 3 AND owner_party_id IN
(
SELECT party_id FROM hz_parties WHERE party_number=p_es_customer_number
);

ELSIF p_es_customer_number IS NOT NULL and p_es_site_number IS NOT NULL THEN

BEGIN
SELECT 
es_customer_name,
es_customer_number,
--es_site_number,
decode(es_customer_status,'A','Active','I','Inactive',es_customer_status)
INTO  
p_es_customer_name,
p_es_customer_number,
--p_es_site_number,
p_es_customer_status
FROM ncr_es_hackathon_v 
WHERE 
es_customer_number = p_es_customer_number 
and rownum=1;
---AND es_site_number = p_es_site_number;
EXCEPTION
WHEN OTHERS THEN
p_es_customer_name := 'Customer not in ES';
p_es_customer_number := 0;
p_es_customer_status := 0;
END;

BEGIN
SELECT 
es_customer_name,
es_customer_number,
es_site_number,
decode(es_customer_status,'A','Active','I','Inactive',es_customer_status)--,
--decode(es_site_status,'A','Active','I','Inactive',es_site_status)
INTO  
p_es_customer_name,
p_es_customer_number,
p_es_site_number,
p_es_customer_status--,
--p_es_site_status
FROM ncr_es_hackathon_v v
WHERE 
es_customer_number = p_es_customer_number 
AND es_site_number = p_es_site_number;
--p_site_count := 1 ;

SELECT count(es_site_number)
INTO p_site_count
FROM ncr_es_hackathon_v WHERE es_customer_number = p_es_customer_number and es_site_number = p_es_site_number and  es_site_status = 'A';

SELECT count(es_site_number)
INTO p_es_site_status
FROM ncr_es_hackathon_v WHERE es_customer_number = p_es_customer_number and es_site_number = p_es_site_number and es_site_status = 'I';

EXCEPTION
WHEN OTHERS THEN
p_es_site_number := 'Site not in ES';
--p_es_site_status := 'Site not in ES';
p_site_count := 0;
p_es_site_status := 0;
END;

SELECT count(1) INTO p_ib_count
FROM csi_item_instances WHERE instance_status_id= 3 AND owner_party_id IN
(
SELECT party_id FROM hz_parties WHERE party_number = p_es_customer_number
)
AND install_location_id IN 
(
SELECT party_site_id FROM hz_party_sites WHERE party_site_number = p_es_site_number
);

END IF;

EXCEPTION 
WHEN OTHERS THEN
p_es_customer_name := 'Customer not in ES';
p_es_customer_number := 0;
p_es_site_number := 0;
p_es_customer_status := 0;
p_es_site_status := 0;
END;


END ncr_es_fetch_customer;

END ncr_es_customer_data;
/
