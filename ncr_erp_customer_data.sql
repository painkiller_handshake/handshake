CREATE OR REPLACE PACKAGE ncr_erp_customer_data
as
PROCEDURE ncr_erp_fetch_customer
(
p_erp_customer_name   OUT VARCHAR2,
p_erp_customer_number IN  OUT VARCHAR2,
p_erp_site_number  IN OUT VARCHAR2,
p_erp_customer_status  OUT VARCHAR2,
p_erp_site_status OUT VARCHAR2,
p_site_count OUT NUMBER ,
p_ib_count OUT NUMBER,
p_erp_ou OUT VARCHAR2
);

END ncr_erp_customer_data;
/ 

CREATE OR replace PACKAGE BODY apps.ncr_erp_customer_data AS 

PROCEDURE ncr_erp_fetch_customer
(
p_erp_customer_name   OUT VARCHAR2,
p_erp_customer_number IN  OUT VARCHAR2,
p_erp_site_number  IN OUT VARCHAR2,
p_erp_customer_status  OUT VARCHAR2,
p_erp_site_status OUT VARCHAR2,
p_site_count OUT NUMBER ,
p_ib_count OUT NUMBER,
p_erp_ou OUT VARCHAR2
)
IS
---p_ib_count NUMBER;
BEGIN

BEGIN
IF  p_erp_customer_number IS NOT NULL AND p_erp_site_number IS NULL THEN
SELECT 
erp_customer_name,
erp_customer_number,
null,
decode(erp_customer_status,'A','Active','I','Inactive',erp_customer_status),
--null,
hr.name ||' ('||hr.attribute19 ||')'
INTO  
p_erp_customer_name,
p_erp_customer_number,
p_erp_site_number,
p_erp_customer_status,
--p_erp_site_status,
p_erp_ou
FROM ncr_erp_hackathon_v v,
hr_all_organization_units hr 
WHERE 
erp_customer_number = p_erp_customer_number 
AND hr.organization_id= v.erp_ou
AND rownum=1;

SELECT count(erp_site_number) 
INTO p_site_count
FROM ncr_erp_hackathon_v WHERE erp_customer_number = p_erp_customer_number AND erp_site_status = 'A';

SELECT count(erp_site_number) 
INTO p_erp_site_status
FROM ncr_erp_hackathon_v WHERE erp_customer_number = p_erp_customer_number AND erp_site_status = 'I';

SELECT count(1) INTO p_ib_count
FROM csi_item_instances WHERE instance_status_id= 3 AND owner_party_id IN
(
SELECT party_id FROM hz_parties WHERE party_number=p_erp_customer_number
);

ELSIF p_erp_customer_number IS NOT NULL AND p_erp_site_number IS NOT NULL THEN

BEGIN
SELECT 
erp_customer_name,
erp_customer_number,
--erp_site_number,
decode(erp_customer_status,'A','Active','I','Inactive',erp_customer_status),
--decode(erp_site_status,'A','Active','I','Inactive',erp_site_status),
hr.name ||' ('||hr.attribute19 ||')'
INTO  
p_erp_customer_name,
p_erp_customer_number,
--p_erp_site_number,
p_erp_customer_status,
---p_erp_site_status,
p_erp_ou
FROM ncr_erp_hackathon_v v,
hr_all_organization_units hr
WHERE 
erp_customer_number = p_erp_customer_number 
AND hr.organization_id= v.erp_ou and rownum=1;
---AND erp_site_number = p_erp_site_number;
EXCEPTION
WHEN OTHERS THEN
p_erp_customer_name := 'Customer not in ERP';
p_erp_customer_number := 0;
p_erp_customer_status := 0;
END;

BEGIN
SELECT 
erp_customer_name,
erp_customer_number,
erp_site_number,
decode(erp_customer_status,'A','Active','I','Inactive',erp_customer_status),
---decode(erp_site_status,'A','Active','I','Inactive',erp_site_status),
hr.name ||' ('||hr.attribute19 ||')'
INTO  
p_erp_customer_name,
p_erp_customer_number,
p_erp_site_number,
p_erp_customer_status,
---p_erp_site_status,
p_erp_ou
FROM ncr_erp_hackathon_v v,
hr_all_organization_units hr
WHERE 
erp_customer_number = p_erp_customer_number 
AND hr.organization_id= v.erp_ou
AND erp_site_number = p_erp_site_number;
--p_site_count := 1 ;

SELECT count(erp_site_number) 
INTO p_erp_site_status
FROM ncr_erp_hackathon_v WHERE erp_customer_number = p_erp_customer_number 
AND erp_site_number = p_erp_site_number
AND erp_site_status = 'I';

SELECT count(erp_site_number) 
INTO p_site_count
FROM ncr_erp_hackathon_v WHERE erp_customer_number = p_erp_customer_number 
AND erp_site_number = p_erp_site_number
AND erp_site_status = 'A';



EXCEPTION
WHEN OTHERS THEN
p_erp_site_number := 'Site not in ERP';
p_erp_site_status := '0' ; --'Site not in ERP';
p_site_count := 0;
END;




SELECT count(1) INTO p_ib_count
FROM csi_item_instances WHERE instance_status_id= 3 AND owner_party_id IN
(
SELECT party_id FROM hz_parties WHERE party_number = p_erp_customer_number
)
AND install_location_id IN 
(
SELECT party_site_id FROM hz_party_sites WHERE party_site_number = p_erp_site_number
);

END IF;

EXCEPTION 
WHEN OTHERS THEN
p_erp_customer_name := 'Customer not in ERP';
p_erp_customer_number := 0;
p_erp_site_number := 0;
p_erp_customer_status := 0;
p_erp_site_status := 0;
END;


END ncr_erp_fetch_customer;

END ncr_erp_customer_data;
/
